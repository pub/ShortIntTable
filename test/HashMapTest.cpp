#include "UnitTest.h"
#include "CPUCycleClock.h"
#include "ShortIntTable.h"

#include <tsl/robin_map.h>
#include <ankerl/unordered_dense.h>
#include <ankerl/robin_hood.h>
#include <sparsehash/dense_hash_map>

#include <assert.h>
#include <fstream>
#include <unordered_map>

TEST_CASE( "ShortStrTable" )
{
    SECTION( "Map" )
    {
        using ShortIntMap = jz::ShortIntTable<uint16_t, int>;
        jz::ShortIntTable<uint16_t, int> m( 100, 20000 );
        REQUIRE( m.emplace( 20, 200 ).second );
        REQUIRE( !m.emplace( 20, 250 ).second );
        REQUIRE( m.emplace( 40, 400 ).second );

        REQUIRE( 200 == m.find( 20 )->second );
    }
    SECTION( "Set" )
    {
        using ShortIntSet = jz::ShortIntTable<uint16_t>;
        ShortIntSet m( 100, 20000 );
        REQUIRE( m.emplace( 20 ).second );
    }
}


std::string trim( const std::string &s )
{
    int i = 0;
    while ( std::isspace( s[i] ) )
        ++i;
    int j = s.length();
    while ( j > i && std::isspace( s[j - 1] ) )
        --j;
    return s.substr( i, j - i );
}

// encode max NChars letters of A-Z
template<size_t NChars = 5>
struct UpperLettersEncoder
{
    static_assert( NChars <= 6, "uint32_t holds max 6 chars!" );

    static constexpr size_t   NBITS_PER_CHAR = 5;
    static constexpr uint32_t CHAR_MASK      = ( uint32_t( 1 ) << NBITS_PER_CHAR ) - 1;

    static uint32_t encode( std::string_view s )
    {
        ASSERT( s.size() <= NChars );

        uint32_t res = 0;
        size_t   pos = 0;
        for ( auto c : s )
        {
            ASSERT( c >= 'A' && c <= 'Z' );
            res |= ( c - 'A' + 1 ) << pos;
            pos += NBITS_PER_CHAR;
        }
        return res;
    }

    static std::string decode( uint32_t val )
    {
        std::string res;
        while ( val )
        {
            char c = ( val & CHAR_MASK ) + 'A' - 1;
            res += c;
            val >> NBITS_PER_CHAR;
        }
        return res;
    }
};


std::unordered_map<std::string, std::string> loadSymbolTable( std::vector<std::string> &symbols )
{
    std::unordered_map<std::string, std::string> m;

    std::ifstream ifs( "../../stocksymbols.csv" );
    ASSERT( ifs.is_open() );
    std::string aline;
    size_t      lineCount = 0;
    while ( std::getline( ifs, aline ) )
    {
        ++lineCount;
        if ( auto n = aline.find( ',' ); n != std::string::npos )
        {
            auto s = aline.substr( 0, n );
            s      = trim( s );
            std::for_each( s.begin(), s.end(), []( char &c ) { c = std::toupper( c ); } );
            if ( s == "SYMBOL" ) // first line
                continue;
            if ( std::all_of( s.begin(), s.end(), []( char c ) { return std::isalpha( c ); } ) )
            {
                ASSERT( s.length() <= 5 );
                auto res = m.emplace( s, aline );
                ASSERT( res.second );
                if ( res.second )
                {
                    symbols.push_back( s );
                }
            }
        }
    }
    return m;
}


static constexpr size_t MAX_SYMBOL_LEN = 5;
using Encoder                          = UpperLettersEncoder<MAX_SYMBOL_LEN>;

struct HashMapTestFixture
{
    std::vector<std::string>                     symbols;
    std::unordered_map<std::string, std::string> symbolMap;

    HashMapTestFixture()
    {
        symbolMap = loadSymbolTable( symbols );
        std::cout << "Added symbols: " << symbolMap.size() << ", bucket_count: " << symbolMap.bucket_count() << std::endl;
    }

    template<class Map>
    Map loadMap() const
    {
        using ValueType = typename Map::value_type;
        using KeyType   = typename Map::key_type;

        Map amap;
        if constexpr ( std::is_same_v<Map, google::dense_hash_map<uint32_t, std::string>> )
            amap.set_empty_key( 0 );
        else if constexpr ( std::is_same_v<Map, google::dense_hash_map<std::string, std::string>> )
            amap.set_empty_key( std::string{} );

        int kSymbol = 0;
        for ( auto &s : symbols )
        {
            if constexpr ( std::is_same_v<KeyType, std::string> )
            {
                auto res = amap.insert( ValueType( s, symbolMap.find( s )->second ) );
                ASSERT( res.second );
            }
            else
            {
                uint32_t key = Encoder::encode( symbols[kSymbol++] );
                auto     res = amap.insert( ValueType( key, symbolMap.find( s )->second ) );
                ASSERT( res.second );
            }
        }
        return amap;
    }

    template<class Map>
    void testLookup( Map &amap, std::string name ) const
    {
        using KeyType  = typename Map::key_type;
        size_t output  = 0;
        int    kSymbol = -1;
        jz::benchmark_median(
                name,
                symbols.size(),
                jz::GetStdSteadyClockTimestamp,
                [&]
                {
                    if constexpr ( std::is_same_v<KeyType, std::string> )
                        output |= amap.find( symbols[kSymbol] )->second.size();
                    else
                        output |= amap.find( Encoder::encode( symbols[kSymbol] ) )->second.size();
                },
                [&] { kSymbol++; } );
    }

    void test_lookup_maps()
    {
        std::cout << "=============================================================\n";
        jz::benchmark_median( "HashMap Lookup Test", symbols.size(), jz::GetStdSteadyClockTimestamp, nullptr ); // print header
        {
            auto mapi = loadMap<std::unordered_map<uint32_t, std::string>>();
            testLookup( mapi, "std::unordered_map int         " );
            auto maps = loadMap<std::unordered_map<std::string, std::string>>();
            testLookup( maps, "std::unordered_map str         " );
        }

        {
            auto mapi = loadMap<jz::ShortIntTable<uint32_t, std::string>>();
            testLookup( mapi, "std::ShortIntTable int         " );
        }
        {
            auto mapi = loadMap<ankerl::unordered_dense::map<uint32_t, std::string>>();
            testLookup( mapi, "ankerl::unordered_dense::map int" );
            auto maps = loadMap<ankerl::unordered_dense::map<std::string, std::string>>();
            testLookup( maps, "ankerl::unordered_dense::map int" );
        }
        {
            auto mapi = loadMap<robin_hood::unordered_flat_map<uint32_t, std::string>>();
            testLookup( mapi, "robin_hood::unordered_flat_map int" );
            auto maps = loadMap<robin_hood::unordered_flat_map<std::string, std::string>>();
            testLookup( maps, "robin_hood::unordered_flat_map int" );
        }
        {
            auto mapi = loadMap<google::dense_hash_map<uint32_t, std::string>>();
            testLookup( mapi, "google::dense_hash_map int      " );
            auto maps = loadMap<google::dense_hash_map<std::string, std::string>>();
            testLookup( maps, "google::dense_hash_map str      " );
        }
    }
};


TEST_CASE( "HashMap-lookup-benchmark" )
{
    HashMapTestFixture f;
    f.test_lookup_maps();

    std::vector<std::string>                     symbols;
    std::unordered_map<std::string, std::string> m = loadSymbolTable( symbols );

    std::cout << "Added symbols: " << m.size() << ", bucket_count: " << m.bucket_count() << std::endl;

    std::cout << "=============================================================\n";
    jz::benchmark_median( "HashMap Lookup Tests", symbols.size(), jz::GetStdSteadyClockTimestamp, nullptr ); // print header

    //-- perfTest std::unordered_map int
    auto testMap = [&]( auto &amap, std::string name )
    {
        using Map       = std::remove_reference_t<decltype( amap )>;
        using KeyType   = typename Map::key_type;
        using ValueType = typename Map::value_type;
        size_t output   = 0;
        int    kSymbol  = 0;
        for ( auto &s : symbols )
        {
            if constexpr ( std::is_same_v<KeyType, std::string> )
            {
                auto res = amap.insert( ValueType( s, m.find( s )->second ) );
                ASSERT( res.second );
            }
            else
            {
                uint32_t key = Encoder::encode( symbols[kSymbol++] );
                auto     res = amap.insert( ValueType( key, m.find( s )->second ) );
                ASSERT( res.second );
            }
        }

        // std::cout << name << " : bucket_count: " << amap.bucket_count() << std::endl;

        kSymbol = -1;
        jz::benchmark_median(
                name.c_str(),
                symbols.size(),
                jz::GetStdSteadyClockTimestamp,
                [&]
                {
                    if constexpr ( std::is_same_v<KeyType, std::string> )
                        output |= amap.find( symbols[kSymbol] )->second.size();
                    else
                        output |= amap.find( Encoder::encode( symbols[kSymbol] ) )->second.size();
                },
                [&] { kSymbol++; } );
    };

    {
        std::unordered_map<uint32_t, std::string>    mapi;
        std::unordered_map<std::string, std::string> maps;
        testMap( mapi, "std::unordered_map int        " );
        testMap( maps, "std::unordered_map str        " );
    }

    {
        jz::ShortIntTable<uint32_t, std::string> mapi;
        testMap( mapi, "std::ShortIntTable int        " );
    }

    {
        ankerl::unordered_dense::map<uint32_t, std::string>    mapi;
        ankerl::unordered_dense::map<std::string, std::string> maps;
        testMap( mapi, "ankerl::unordered_dense::map int" );
        testMap( maps, "ankerl::unordered_dense::map str" );
    }
    {
        robin_hood::unordered_flat_map<uint32_t, std::string>    mapi;
        robin_hood::unordered_flat_map<std::string, std::string> maps;
        testMap( mapi, "robin_hood::unordered_flat_map int" );
        testMap( maps, "robin_hood::unordered_flat_map str" );
    }

    {
        tsl::robin_map<uint32_t, std::string>    mapi;
        tsl::robin_map<std::string, std::string> maps;
        testMap( mapi, "tsl::robin_map int             " );
        testMap( maps, "tsl::robin_map str             " );
    }
    {
        google::dense_hash_map<uint32_t, std::string> mapi;
        mapi.set_empty_key( 0 );
        google::dense_hash_map<std::string, std::string> maps;
        maps.set_empty_key( std::string{} );
        testMap( mapi, "google::dense_hash_map int     " );
        testMap( maps, "google::dense_hash_map str     " );
    }
}

/*
Added symbols: 7833, bucket_count: 10273
=============================================================
HashMap Lookup Tests nSamples=7833                Min     50%     99%    Max
std::unordered_map int                            28      46      144     573
std::unordered_map str                            31      51      234     14162
std::ShortIntTable int                            24      41      217     568
ankerl::unordered_dense::map int                  14      20      33      15897
ankerl::unordered_dense::map str                  12      14      19      152
tsl::robin_map int                                19      39      170     899
tsl::robin_map str                                26      41      115     505
google::dense_hash_map int                        12      18      24      53
google::dense_hash_map str                        19      24      29      65
0.102 s: HashMap-lookup-benchmark

*/