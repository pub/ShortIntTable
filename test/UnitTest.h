
#ifdef ADD_TEST_CASE_AS_FUNCTION
#define ADD_TEST_CASE( name ) void name()
#else
#define ADD_TEST_CASE( name ) TEST_CASE( #name )
#endif

#define ASSERT( expr )                                                                                                                              \
    do                                                                                                                                              \
    {                                                                                                                                               \
        if ( expr )                                                                                                                                 \
        {                                                                                                                                           \
        }                                                                                                                                           \
        else                                                                                                                                        \
        {                                                                                                                                           \
            std::cerr << '\n' << __FILE__ << ":" << __LINE__ << ": " << __FUNCTION__ << ": Assertion failed: " #expr "\n";                          \
            abort();                                                                                                                                \
        }                                                                                                                                           \
    } while ( false )

#define ASSERT_OP( a, OP, b )                                                                                                                       \
    do                                                                                                                                              \
    {                                                                                                                                               \
        auto _a = ( a );                                                                                                                            \
        auto _b = ( b );                                                                                                                            \
        if ( a OP b )                                                                                                                               \
        {                                                                                                                                           \
        }                                                                                                                                           \
        else                                                                                                                                        \
        {                                                                                                                                           \
            std::cerr << '\n'                                                                                                                       \
                      << __FILE__ << ":" << __LINE__ << ": " << __FUNCTION__ << ": Assertion failed: " << #a << " " << #OP << " " << #b             \
                      << ". Evaluated: " << _a << " " << #OP << " " << _b << ".\n";                                                                 \
            abort();                                                                                                                                \
        }                                                                                                                                           \
    } while ( false )

#define ASSERT_EQ( a, b ) ASSERT_OP( a, ==, b )
#define ASSERT_NE( a, b ) ASSERT_OP( a, !=, b )
#define ASSERT_LT( a, b ) ASSERT_OP( a, <, b )
#define ASSERT_LE( a, b ) ASSERT_OP( a, <=, b )

#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include "catch.hpp"
