#pragma once

/************************************************************************************
class CPUCycleClock provide low-cost clock functions to get the current nano seconds since epoch.
************************************************************************************/

#include <sys/time.h>
#include <sched.h>
#include <pthread.h>

#include <vector>
#include <iostream>
#include <chrono>
#include <numeric>
#include <string>
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <thread>

namespace jz
{

class IClock
{
public:
    virtual ~IClock() = default;

    virtual int64_t nowNanos() const = 0;
};

class StdSteadyClock : public IClock
{
public:
    virtual int64_t nowNanos() const
    {
        return std::chrono::steady_clock::now().time_since_epoch().count();
    }
};
class StdSystemClock : public IClock
{
public:
    virtual int64_t nowNanos() const
    {
        return std::chrono::system_clock::now().time_since_epoch().count();
    }
};

static const StdSteadyClock stdSteadyClock;
static const StdSystemClock stdSystemClock;

inline int set_cpu_affinity( int iCPU, int pid = 0 )
{
    cpu_set_t mask;
    CPU_ZERO( &mask );
    CPU_SET( iCPU, &mask );
    return sched_setaffinity( pid, sizeof( mask ), &mask );
}
inline int set_cpu_affinity( std::thread &athread, int iCPU )
{
    cpu_set_t cpuset;
    CPU_ZERO( &cpuset );
    CPU_SET( iCPU, &cpuset );
    return pthread_setaffinity_np( athread.native_handle(), sizeof( cpu_set_t ), &cpuset );
}

inline uint64_t get_tsc_ticks()
{
    // from https://github.com/google/benchmark/blob/v1.1.0/src/cycleclock.h
#if defined( BENCHMARK_OS_MACOSX )
    // this goes at the top because we need ALL Macs, regardless of
    // architecture, to return the number of "mach time units" that
    // have passed since startup.  See sysinfo.cc where
    // InitializeSystemInfo() sets the supposed cpu clock frequency of
    // macs to the number of mach time units per second, not actual
    // CPU clock frequency (which can change in the face of CPU
    // frequency scaling).  Also note that when the Mac sleeps, this
    // counter pauses; it does not continue counting, nor does it
    // reset to zero.
    return mach_absolute_time();
#elif defined( __i386__ )
    int64_t ret;
    __asm__ volatile( "rdtsc" : "=A"( ret ) );
    return ret;
#elif defined( __x86_64__ ) || defined( __amd64__ )
    uint64_t low, high;
    __asm__ volatile( "rdtsc" : "=a"( low ), "=d"( high ) );
    return ( high << 32 ) | low;
#elif defined( __powerpc__ ) || defined( __ppc__ )
    // This returns a time-base, which is not always precisely a cycle-count.
    int64_t tbl, tbu0, tbu1;
    asm( "mftbu %0" : "=r"( tbu0 ) );
    asm( "mftb  %0" : "=r"( tbl ) );
    asm( "mftbu %0" : "=r"( tbu1 ) );
    tbl &= -static_cast<int64>( tbu0 == tbu1 );
    // high 32 bits in tbu1; low 32 bits in tbl  (tbu0 is garbage)
    return ( tbu1 << 32 ) | tbl;
#elif defined( __sparc__ )
    int64_t tick;
    asm( ".byte 0x83, 0x41, 0x00, 0x00" );
    asm( "mov   %%g1, %0" : "=r"( tick ) );
    return tick;
#elif defined( __ia64__ )
    int64_t itc;
    asm( "mov %0 = ar.itc" : "=r"( itc ) );
    return itc;
#elif defined( COMPILER_MSVC ) && defined( _M_IX86 )
    // Older MSVC compilers (like 7.x) don't seem to support the
    // __rdtsc intrinsic properly, so I prefer to use _asm instead
    // when I know it will work.  Otherwise, I'll use __rdtsc and hope
    // the code is being compiled with a non-ancient compiler.
    _asm rdtsc
#elif defined( COMPILER_MSVC )
    return __rdtsc();
#elif defined( __aarch64__ )
    // System timer of ARMv8 runs at a different frequency than the CPU's.
    // The frequency is fixed, typically in the range 1-50MHz.  It can be
    // read at CNTFRQ special register.  We assume the OS has set up
    // the virtual timer properly.
    int64_t virtual_timer_value;
    asm volatile( "mrs %0, cntvct_el0" : "=r"( virtual_timer_value ) );
    return virtual_timer_value;
#elif defined( __ARM_ARCH )
#if ( __ARM_ARCH >= 6 ) // V6 is the earliest arch that has a standard cyclecount
    uint32_t pmccntr;
    uint32_t pmuseren;
    uint32_t pmcntenset;
    // Read the user mode perf monitor counter access permissions.
    asm volatile( "mrc p15, 0, %0, c9, c14, 0" : "=r"( pmuseren ) );
    if ( pmuseren & 1 )
    { // Allows reading perfmon counters for user mode code.
        asm volatile( "mrc p15, 0, %0, c9, c12, 1" : "=r"( pmcntenset ) );
        if ( pmcntenset & 0x80000000ul )
        { // Is it counting?
            asm volatile( "mrc p15, 0, %0, c9, c13, 0" : "=r"( pmccntr ) );
            // The counter is set up to count every 64th cycle
            return static_cast<int64_t>( pmccntr ) * 64; // Should optimize to << 6
        }
    }
#endif
    struct timeval tv;
    gettimeofday( &tv, nullptr );
    return static_cast<int64_t>( tv.tv_sec ) * 1000000 + tv.tv_usec;
#elif defined( __mips__ )
    // mips apparently only allows rdtsc for superusers, so we fall
    // back to gettimeofday.  It's possible clock_gettime would be better.
    struct timeval tv;
    gettimeofday( &tv, nullptr );
    return static_cast<int64_t>( tv.tv_sec ) * 1000000 + tv.tv_usec;
#else
// The soft failover to a generic implementation is automatic only for ARM.
// For other platforms the developer is expected to make an attempt to create
// a fast implementation and use generic version if nothing better is available.
#error You need to define CycleTimer for your OS and CPU
#endif
}


#if defined( __GNUC__ ) && defined( __x86_64__ )
inline uint64_t get_tsc_ticks_p( int &chip, int &core )
{
    uint32_t countlo, counthi;
    uint32_t chx; // Set to processor signature register - set to chip/socket & core ID by recent Linux kernels.

    __asm__ volatile( "RDTSCP" : "=a"( countlo ), "=d"( counthi ), "=c"( chx ) );
    chip = ( chx & 0xFFF000 ) >> 12;
    core = ( chx & 0xFFF );
    return ( uint64_t( counthi ) << 32 ) | countlo;
}
#endif

inline uint64_t GetStdSteadyClockTimestamp()
{
    return std::chrono::steady_clock::now().time_since_epoch().count();
}

/// \intervalInNanos the interval for number of CPU cycles, default 1000 ns or 1 us.
/// return CPUCycles/microsec
inline size_t calculateCPUFreq( const size_t NSamples = 100, bool bPrintStats = true, size_t intervalInNanos = 1000 )
{
    const size_t        NLOOPS = 10000; // when CPU freq 2.6G, NSamples = 1000, NLOOPS = 10000. The test takes ~2 second.
    std::vector<size_t> ticks( NSamples ), timestamps( NSamples ), ticksPerMicro( NSamples - 1 );
    size_t              sum = rand();

    auto sleep = [&]
    {
        for ( size_t k = 0; k < NLOOPS; ++k )
            sum += rand() * rand();
    };
#if defined( __GNUC__ ) && defined( __x86_64__ )
    int  chip, core;
    auto get_ticks = [&] { return get_tsc_ticks_p( chip, core ); };
#endif
    uint64_t tick0 = get_tsc_ticks(), tick1;

    for ( size_t i = 0; i < NSamples / 2; ++i )
    {
        ticks[i]      = get_tsc_ticks();
        timestamps[i] = GetStdSteadyClockTimestamp();
        sleep();
    }
    for ( size_t i = NSamples / 2; i < NSamples; ++i )
    {
        timestamps[i] = GetStdSteadyClockTimestamp();
        ticks[i]      = get_tsc_ticks();
        sleep();
    }
    {
#if defined( __GNUC__ ) && defined( __x86_64__ )
        int  chip, core;
        auto get_ticks = [&] { return get_tsc_ticks_p( chip, core ); };
        int  chip1, core1;
        tick1 = get_tsc_ticks_p( chip1, core1 );
        if ( chip1 != chip || core1 != core )
        {
            //            std::cout << "calculateCPUFreq Chip/Core changed while testing: from (chip:" << chip << " core:" << core << ") to (chip:" << chip1
            //                      << " core:" << core1 << ")\n";
        }
#endif
    }
    if ( sum == 12356 ) // less likely
    {
        std::cout << "calculateCPUFreq sum:" << sum << "\n";
    }
    // calc
    for ( size_t i = 1; i < NSamples; ++i )
        ticksPerMicro[i - 1] = ( ticks[i] - ticks[i - 1] ) * intervalInNanos / ( timestamps[i] - timestamps[i - 1] );

    std::sort( ticksPerMicro.begin(), ticksPerMicro.end() );
    size_t N = ticksPerMicro.size();
    if ( bPrintStats )
    {
        std::cout << "Time elapse(ms):" << ( tick1 - tick0 ) / ticksPerMicro[N / 2] / 1000
#if defined( __GNUC__ ) && defined( __x86_64__ )
                  << " on Chip:" << chip << ", Core:" << core
#endif
                  << ", nSamples:" << NSamples << ", CPUCycles/Microsec: min:" << ticksPerMicro[0] << ", 50%:" << ticksPerMicro[N / 2]
                  << ", 90%:" << ticksPerMicro[N * 0.9] << ", 99%:" << ticksPerMicro[std::min( size_t( N * 0.99 ), N - 1 )]
                  << ", max:" << ticksPerMicro.back() << "\n";
    }
    return ticksPerMicro[ticksPerMicro.size() / 2];

    // Time elapse(ms):188 on Chip:0, Core:1, nSamples:1000, CPUCycles/Microsec: min:2556, 50%:2594, 90%:2595, 99%:2600, max:2630
}

/// calculate GetStdSteadyClockTimestamp time consumption in CPU cycles.
inline size_t calcGetCurrentTimeCost()
{
    const size_t        NSamples = 10000;
    std::vector<size_t> nowTs( NSamples ), nowCosts( NSamples ), tickCosts( NSamples );

    for ( size_t i = 0; i < NSamples / 2; ++i )
    {
        nowTs[i]    = GetStdSteadyClockTimestamp();
        nowCosts[i] = get_tsc_ticks();
    }
    for ( size_t i = NSamples / 2; i < NSamples; ++i )
    {
        nowCosts[i] = get_tsc_ticks();
        nowTs[i]    = GetStdSteadyClockTimestamp();
    }
    for ( size_t i = 0; i < NSamples; ++i )
        tickCosts[i] = get_tsc_ticks();

    for ( size_t i = 0; i < NSamples - 1; ++i )
    {
        nowCosts[i]  = nowCosts[i + 1] - nowCosts[i];
        tickCosts[i] = tickCosts[i + 1] - tickCosts[i];
    }
    // remove last
    size_t N = NSamples - 1;
    nowCosts.erase( --nowCosts.end() );
    tickCosts.erase( --tickCosts.end() );


    if ( std::accumulate( nowTs.begin(), nowTs.end(), size_t( 0 ) ) == 1234 )
    {
        std::cout << "calcGetCurrentTimeCost unlikely happen\n";
    }
    std::cout << "StdNow costs (ticks): " << nowCosts[N / 2] - tickCosts[N / 2] << ", GetTicks costs(ticks): " << tickCosts[N / 2] << "\n";
    return nowCosts[N / 2] - tickCosts[N / 2];

    // StdNow costs (ticks): 20, GetTicks costs(ticks): 24
}

/// CPUCycleClock implements:
///     int64_t now() // return nanoseconds. There's some overhead to convert CPU cycles to nanos.
///     int64_t nowCPUCycles() // CPU ticks/cycles.
///
/// A typical way to calculate duration:
///     int64_t t0 = cpuCycleSteadyClock.nowCPUCycles();
///     do some work....
///     int64_t t1 = cpuCycleSteadyClock.nowCPUCycles();
///     int64_t durationNanos = cpuCycleSteadyClock.convertDurationCPUCyclesToNanos(t1-t0);
template<class StdClockT = std::chrono::system_clock>
class CPUCycleClock : IClock
{
public:
    using StdClock = StdClockT;

protected:
    static constexpr int64_t FREQ_INTERVAL_POWER2 = 10; // 1024 nanos

    int64_t m_ticksPerMicro = 0;
    int64_t m_initialTicks  = 0;
    int64_t m_initialNanos  = 0;

public:
    CPUCycleClock( bool bCalibrate )
    {
        if ( bCalibrate )
            calibrate();
    }
    // using calculateCPUFreq to init state.
    // It's NOT thread safe. It should be used within a thread which pinned to a CPU core.
    void calibrate()
    {
        m_ticksPerMicro = calculateCPUFreq( 1000, false, int64_t( 1 ) << FREQ_INTERVAL_POWER2 );

        auto ts1 = StdClock::now().time_since_epoch().count();
        auto ts2 = StdClock::now().time_since_epoch().count();
        auto tk  = get_tsc_ticks();
        auto ts3 = StdClock::now().time_since_epoch().count();
        auto ts4 = StdClock::now().time_since_epoch().count();

        m_initialTicks = tk;
        m_initialNanos = ( ( ts1 + ( ts4 - ts1 ) / 2 ) + ( ts2 + ( ts3 - ts2 ) / 2 ) ) / 2;
    }
    bool inited() const
    {
        return m_ticksPerMicro != 0;
    }
    int64_t CPUCyclesPerMicros() const
    {
        return m_ticksPerMicro;
    }
    /// \return CPU cycles.
    int64_t nowCPUCycles() const
    {
        return get_tsc_ticks();
    }
    // \return nanoseconds since epoch from StdClockT.
    std::chrono::nanoseconds getStdClockNow() const
    {
        return StdClock::now().time_since_epoch();
    }
    /// return time point
    int64_t convertTimestampCPUCyclesToNanos( int64_t timestampCPUCycles ) const
    {
        return convertDurationCPUCyclesToNanos( timestampCPUCycles - m_initialTicks ) + m_initialNanos;
    }
    /// return time duration
    int64_t convertDurationCPUCyclesToNanos( int64_t durationCPUCycles ) const
    {
        return ( ( durationCPUCycles << FREQ_INTERVAL_POWER2 ) / m_ticksPerMicro );
    }
    /// \return time point in nanos. It's slow, not recommended to use.
    int64_t nowNanos() const override
    {
        return ( ( ( get_tsc_ticks() - m_initialTicks ) << FREQ_INTERVAL_POWER2 ) / m_ticksPerMicro ) + m_initialNanos;
    }
    /// \return nanoseconds since epoch.
    std::chrono::nanoseconds nowSinceEpoch() const
    {
        return std::chrono::nanoseconds{ ( ( ( get_tsc_ticks() - m_initialTicks ) << FREQ_INTERVAL_POWER2 ) / m_ticksPerMicro ) + m_initialNanos };
    }
};

inline CPUCycleClock<std::chrono::system_clock> cpuCycleSystemClock( true );
inline CPUCycleClock<std::chrono::steady_clock> cpuCycleSteadyClock( true );

/// \brief if name == nullptr, no output.
/// \tparam Func The function to test. If it's nullptr_t, only print the header line.
/// \tparam GetTimeStampFuncT can be GetStdSteadyClockTimestamp, cpuCycleSystemClock, or cpuCycleSystemClock.
template<class GetTimeStampFuncT, class Func, class PreFunc = std::nullptr_t>
size_t benchmark_median( const std::string       &name,
                         const size_t             NSAMPLES,
                         const GetTimeStampFuncT &GetTimeStampFunc,
                         Func                   &&func,
                         PreFunc                &&preFunc         = PreFunc{},
                         bool                     bPrintQuantiles = false )
{
    const bool usingCPUCycleClock = std::is_same_v<GetTimeStampFuncT, CPUCycleClock<std::chrono::system_clock>> ||
                                    std::is_same_v<GetTimeStampFuncT, CPUCycleClock<std::chrono::steady_clock>>;

    if constexpr ( std::is_same_v<Func, std::nullptr_t> )
    {
        std::cout << name << " nSamples=" << NSAMPLES << "  \t\t  Min  \t  50%  \t  99%  \t Max" << std::endl;
        return 0;
    }
    else
    {
        const size_t NWARMUPS = 0;
        struct SampleInfo
        {
            size_t idx;
            size_t duration;
            size_t timeStamp;
        };
        std::vector<SampleInfo> samples( NSAMPLES ); // [index: time]

        size_t timeStart;
        if constexpr ( usingCPUCycleClock )
        {
            timeStart = GetTimeStampFunc.nowCPUCycles();
        }
        else
            timeStart = GetTimeStampFunc();
        size_t timeElapse;
        for ( size_t i = 0; i < NSAMPLES + NWARMUPS; ++i )
        {
            if constexpr ( !std::is_same_v<PreFunc, std::nullptr_t> )
                preFunc();
            size_t t0;
            if constexpr ( usingCPUCycleClock )
            {
                t0 = GetTimeStampFunc.nowCPUCycles();
                func();
                timeElapse = GetTimeStampFunc.nowCPUCycles() - t0;
            }
            else
            {
                t0 = GetTimeStampFunc();
                func();
                timeElapse = GetTimeStampFunc() - t0;
            }

            if ( i >= NWARMUPS )
                samples[i - NWARMUPS] = SampleInfo{ ( i - NWARMUPS ), timeElapse, t0 - timeStart };
        }
        if constexpr ( usingCPUCycleClock )
        {
            for ( auto &s : samples )
                s.duration = GetTimeStampFunc.convertDurationCPUCyclesToNanos( s.duration );
        }
        std::sort( samples.begin(), samples.end(), [&]( const SampleInfo &a, const SampleInfo &b ) { return a.duration < b.duration; } );

        if ( !name.empty() )
        {
            if ( bPrintQuantiles )
                std::cout << name << " nSamples:" << NSAMPLES << ", MinTime:" << samples[0].duration << ", 50%:" << samples[NSAMPLES / 2].duration
                          << ", 99%:" << samples[NSAMPLES * 0.99].duration << ", Max: " << samples.back().duration;
            else
                std::cout << name << "  \t\t  " << samples[0].duration << "  \t  " << samples[NSAMPLES / 2].duration << "  \t  "
                          << samples[NSAMPLES * 0.99].duration << "  \t  " << samples.back().duration;

            std::cout << std::endl;
        }

        return samples[NSAMPLES / 2].duration;
    }
}

struct PerfTestBase
{
    void constructTest()
    {
    }
    void destructTest()
    {
    }
    void prepareTest()
    {
    }
    // void doTest()
};

/// \tparam TestFrameWorkT concept:
///   void constructTest();
///   void destructTest();
///   void prepareTest(); // invoke to prepare for sampling work.
///   void doTest();  // invoke to get a sample.
template<class TestFrameWorkT, uint64_t ( *GetTimestampFunc )() = &GetStdSteadyClockTimestamp>
void run_perftest_framewwork( const char *testname, const size_t nRepeatTests = 5, const size_t nSamplesPerTest = 1000 )
{
    TestFrameWorkT testFrameWork;

    if ( GetTimestampFunc == &GetStdSteadyClockTimestamp )
        std::cout << "---- Test:  " << testname << " using std::steady_clock (NanoSeconds) ------\n";
    else
        std::cout << "---- Test:  " << testname << " using get_tsc_ticks (CPU Cycles) ------\n";

    for ( auto i = 0ul; i < nRepeatTests; ++i )
    {
        testFrameWork.constructTest();
        benchmark_median(
                "-", nSamplesPerTest, GetTimestampFunc, [&] { testFrameWork.doTest(); }, [&] { testFrameWork.prepareTest(); } );
        testFrameWork.destructTest();
    }
}

template<class Func>
size_t benchmark_avg( const char *name, size_t NSAMPLES, Func &&func )
{
    for ( size_t i = 0; i < 0u; ++i ) // warm up
        func();

    auto timeStart = std::chrono::steady_clock::now();

    for ( size_t i = 0; i < NSAMPLES; ++i )
        func();

    auto timeDiff = std::chrono::steady_clock::now() - timeStart;

    auto res = timeDiff.count() / NSAMPLES;
    if ( name )
        std::cout << name << " time(ns): " << res << ", " << NSAMPLES << " loops time(ns):" << timeDiff.count() << std::endl;
    return res;
}

} // namespace jz
