# ShortIntMap
An efficient map designed for short int key lookups.

## Lookup Test Result.
```
Added symbols: 7833, bucket_count: 10273
=============================================================
HashMap Lookup Tests nSamples=7833                Min     50%     99%    Max
std::unordered_map int                            28      46      144     573
std::unordered_map str                            31      51      234     14162
std::ShortIntTable int                            24      41      217     568
ankerl::unordered_dense::map int                  14      20      33      15897
ankerl::unordered_dense::map str                  12      14      19      152
tsl::robin_map int                                19      39      170     899
tsl::robin_map str                                26      41      115     505
google::dense_hash_map int                        12      18      24      53
google::dense_hash_map str                        19      24      29      65
```