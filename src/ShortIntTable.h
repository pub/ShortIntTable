#pragma once
#include <type_traits>
#include <stdint.h>
#include <vector>
#include <optional>
#include <assert.h>

namespace jz
{

// ShortIntMap is optimized for lookup, but slow to insert/remove.
// The key type is of small integers (eg, int8_t, or int16_t).
// There are 2 vectors: reversed-index vector and value vector.
// All keys are saved in a reversed-index vector, where v[key] is the index of value vector.
template<class K = uint16_t, class V = void>
class ShortIntTable
{
public:
    static_assert( std::is_integral_v<K>, "K must be integral!" );
    static constexpr bool has_mapped_value = std::is_same_v<V, void>;

    using key_type   = K;
    using value_type = std::conditional_t<has_mapped_value, const K, std::pair<const K, V>>;

private:
    using IndexType                          = K;
    static constexpr IndexType INVALID_INDEX = 0;

    struct InternalValueType
    {
        std::optional<value_type> data;

        // if data.has_value(), index is the key; else, index is the next free slot (INVALID_INDEX indicates no next value)
        IndexType keyOrNextFreeSlot; // -1 is the actual index.
    };

public:
    struct iterator
    {
        using Iter = typename std::vector<InternalValueType>::iterator;
        Iter iter, itEnd;

        using value_type = typename ShortIntTable::value_type;

        value_type *operator->()
        {
            assert( iter->data.has_value() );
            return &*iter->data;
        }
        value_type &operator*()
        {
            return *operator->();
        }
        bool operator==( const iterator &a ) const
        {
            return iter == a.iter && itEnd == a.itEnd;
        }
        bool operator!=( const iterator &a ) const
        {
            return !( operator==( a ) );
        }
        iterator &operator=( const iterator &a )
        {
            iter  = a.iter;
            itEnd = a.itEnd;
        }

        iterator &operator++()
        {
            while ( iter != itEnd )
            {
                ++iter;
                if ( iter->data.has_value() )
                    break;
            }
            *this;
        }
        iterator operator++( int )
        {
            iterator res = *this;
            ++( *this );
            return res;
        }
    };

private:
    std::vector<IndexType>         m_indicesByKey; // -1 is the actual index ; 0 indicates no value.
    std::vector<InternalValueType> m_values;
    size_t                         m_size = 0;

    IndexType m_nextFreeSlot = INVALID_INDEX; // the index in values vector. -1 is the actual index;

public:
    /// \param nReserveValues Reserved number of values, whih is used to reserver value vector.
    /// \param estimateMaxKeyValue Estimated max key value, which is used to reserve key vector.
    ShortIntTable( size_t nReserveValues = 0, size_t estimateMaxKeyValue = 0 )
    {
        m_indicesByKey.resize( estimateMaxKeyValue );
        for ( auto &v : m_indicesByKey )
            v = INVALID_INDEX;

        if ( nReserveValues )
        {
            m_values.resize( nReserveValues );
            m_nextFreeSlot = 1;
            for ( auto i = 0U; i < m_values.size(); ++i )
            {
                m_values[i].keyOrNextFreeSlot = i + 1 + 1; // next free slot.
            }
            m_values.back().keyOrNextFreeSlot = INVALID_INDEX;
        }
    }

    /// return <end_iterator, false> if key exists already.
    template<class... Args>
    std::pair<iterator, bool> emplace( K key, Args &&...args )
    {
        if ( key < m_indicesByKey.size() )
        {
            if ( m_indicesByKey[key] != INVALID_INDEX )
                return { iterator{}, false };
        }
        else
        {
            m_indicesByKey.resize( key + 1 );
        }
        // insert value
        if ( m_nextFreeSlot == INVALID_INDEX ) // push a new slot
        {
            m_indicesByKey[key] = m_values.size() + 1;
            m_values.resize( m_values.size() + 1 );
            m_values.back().keyOrNextFreeSlot = key + 1;
            m_values.back().data.emplace( key, std::forward<Args>( args )... );
        }
        else
        {
            m_indicesByKey[key] = m_nextFreeSlot;
            auto &valueSlot     = m_values[m_nextFreeSlot - 1];
            assert( !valueSlot.data.has_value() );
            m_nextFreeSlot = valueSlot.keyOrNextFreeSlot;

            valueSlot.keyOrNextFreeSlot = key + 1;
            valueSlot.data.emplace( key, std::forward<Args>( args )... );
        }
        ++m_size;
        return { iterator{ std::next( m_values.begin(), m_indicesByKey[key] ), m_values.end() }, true };
    }

    template<class... Args>
    std::pair<iterator, bool> insert( const value_type &val )
    {
        return emplace( val.first, val.second );
    }

    /// return nullptr if key is not in container.
    const value_type *getAt( K key ) const
    {
        if ( key >= m_indicesByKey.size() )
            return nullptr;
        if ( auto id = m_indicesByKey[key]; id != INVALID_INDEX )
            return *m_values[id - 1].data;
        return nullptr;
    }
    value_type *getAt( K key )
    {
        if ( key >= m_indicesByKey.size() )
            return nullptr;
        if ( auto id = m_indicesByKey[key]; id != INVALID_INDEX )
            return *m_values[id - 1].data;
        return nullptr;
    }

    int count( K key ) const
    {
        return getAt( key ) ? 1 : 0;
    }

    iterator begin()
    {
        return { m_values.begin(), m_values.end() };
    }
    iterator end()
    {
        return { m_values.end(), m_values.end() };
    }
    size_t size() const
    {
        return m_size;
    }
    size_t bucket_count() const
    {
        return m_indicesByKey.size();
    }

    iterator find( K key )
    {
        if ( key >= m_indicesByKey.size() )
            return end();
        if ( auto id = m_indicesByKey[key]; id != INVALID_INDEX )
            return { std::next( m_values.begin(), id - 1 ), m_values.end() };
        return end();
    }

    void erase( K key )
    {
        if ( key >= m_indicesByKey.size() )
            return;
        if ( auto id = m_indicesByKey[key]; id != INVALID_INDEX )
        {
            m_indicesByKey[key]          = INVALID_INDEX;
            InternalValueType &valueSlot = m_values[id - 1];
            valueSlot.data.reset();
            valueSlot.keyOrNextFreeSlot = m_nextFreeSlot;
            m_nextFreeSlot              = id;
        }
    }
};

} // namespace jz